Source801: https://galaxy.ansible.com/download/ansible-posix-1.5.4.tar.gz
Source901: https://galaxy.ansible.com/download/community-general-7.3.0.tar.gz
Source902: https://galaxy.ansible.com/download/containers-podman-1.10.3.tar.gz

Provides: bundled(ansible-collection(ansible.posix)) = 1.5.4
Provides: bundled(ansible-collection(community.general)) = 7.3.0
Provides: bundled(ansible-collection(containers.podman)) = 1.10.3

Source996: CHANGELOG.rst
Source998: collection_readme.sh
